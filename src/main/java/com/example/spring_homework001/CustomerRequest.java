package com.example.spring_homework001;

public class CustomerRequest {
    private String name;
    private String gender;
    private Integer age;
    private String address;

    public CustomerRequest(String name, Integer age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public CustomerRequest() {

    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public Integer getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
