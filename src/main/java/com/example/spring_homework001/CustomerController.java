package com.example.spring_homework001;

import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;

@RestController
public class CustomerController {
    ArrayList<Customer> customers = new ArrayList<>();
    public int id = 1;
    public CustomerController() {
        customers.add(new Customer(id++, "Sokhim", 21, "#123PP"));
    }

    // get all customer data
    @GetMapping("/customers")
    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    // insert into arraylist
    @PostMapping("/customers")
    public Customer insertCustomer(@RequestBody CustomerRequest customerRequest) {

        Customer customer = new Customer();
        customer.setId(id++);
        customer.setName(customerRequest.getName());
        customer.setGender(customerRequest.getGender());
        customer.setAge(customerRequest.getAge());
        customer.setAddress(customerRequest.getAddress());
        customers.add(customer);
        return customer;
    }

    // get customer by id
    @PostMapping("/customer/{id}")
    public Customer getCustomerById(@PathVariable("id") Integer customId) {

        for (Customer custom : customers) {
            if (custom.getId() == customId) {
                return custom;
            }
        }
        return null;
    }

    // get customer by name

    @GetMapping("/customer/search")
    Customer findCustomerByName(@RequestParam String name) {
        for (Customer custom : customers) {
            if (custom.getName().equals(name)) {
                return custom;
            }
        }
        return null;
    }




}
